---
title: "Chennai Super Kings: The Journey Of A Million Roars"
date: 2014-05-23T19:14:54+01:00
description: "The story of the most successful franchise of Indian Premiere League"
---

![alt text](https://i.imgur.com/qY3FPOe.jpg "Chennai Super Kings")

In the January of 2008, India Cements bought a franchise for $91m (approx INR 360 Crores). The franchise was named “Chennai Super Kings”. Back then, it was supposed to be the fourth most expensive purchases of all. India Cements had bought a lot of stars like Matthew Hayden, Stephen Fleming, Muttiah Muralitharan, Albie Morkel and Suresh Raina to name a few. They bagged the costliest buy of Mahendra Singh Dhoni for $1.5m. It made headlines in the papers the next day. CSK earned a few supporters because of MSD while the rest supported CSK because they wanted to back their city – Chennai. The north-south divide came into play yet again as a few mocked how preposterous the name of the franchise sounded, how much they hated our skipper and how badly we would fare. No one could predict that CSK would be the most successful side of the IPL in years to come. The journey of a million roars began.

After Brendon Mccullum set the IPL on fire with his unbeaten blitzkrieg knock of 158 for the KKR, the journey for CSK too began in the next match as they went on to make the highest total of the season, scoring 240/5 with “Mr. Cricket” Michael Hussey racing away to 116 off 54 deliveries. There was no looking back after that day. While outsiders like Matthew Hayden, Makhaya Ntini, Muttiah Muralitharan, Mike Hussey and Albie Morkel showed their class, the Indian pool of MSD, Suresh Raina, Lakshmipathy Balaji, Subramaniam Badrinath and Joginder Sharma complemented very well to form an incredible mix of youth and veterans. They lost some matches, but won our hearts. Somewhere down the line they changed from being just another team, to namma team.

We raced away to the finals in the first season and took on the underdogs of the season, Rajasthan Royals. We were so close yet so far. We eventually lost the finals but came back harder in the next season’s league matches. The South African pitches did not bother us but the defeat in the semi finals to Royal Challengers Bangalore came as a blow. The third season did not start the way we wanted it to. Halfway through the tournament, a saviour by name of Doug Bollinger arrived. Three local heroes in the form of Ravichandran Ashwin, Murali Vijay and Subramaniam Badrinath emerged. Ashwin kept strangling the batsmen with his wily off-spin, Vijay showed his class with his 127 against RR and Badrinath showed why he was considered the floater of CSK. But the last league game against the Kings XI in Dharamsala was our key to the semi-finals, and that is when MSD, with the foundation laid by Raina and Badri, played an innings which no CSK fan can ever forget; a night when Irfan was launched by our skipper to the cleaners.

CSK beat Deccan Chargers comprehensively and took on one of the favorites of the 2010 edition – Mumbai Indians. Reeling at 68/3, MSD and Raina took us to 168 as we lifted the trophy for the first time after having been written off during the first half of the tournament. We went on to win the Champions League the same year in South Africa as our local boys Vijay and Ashwin won the Golden Bat and Golden Ball respectively. It was an emotional moment for us as fresh auctions were on the cards.

We managed to retain and buy back the core of the team – MSD, Raina, Ashwin, Vijay, Hussey, Bollinger, Morkel, Badrinath. Our prowess was shown yet again as we became the first team to win all home games. We went on to beat RCB in the play offs as well as in the finals and lift the cup for the second consecutive time.

Edition 5 of the IPL saw us welcome a new super king – Ravindra Jadeja. Few of his performances proved why he was a vital cog in our wheel. Season 5 saw us scraping through to the play offs; some called it luck, but luck has a part to play everywhere. We utilized the opportunity we got and beat MI and DD to finally lock horns with KKR in the finals. Fate had different plans for us that day as we could not defend 190+ in our own backyard. In the hope of claiming a hattrick, the horrors of 2008 returned as we lost a close game.

The sixth edition was no different as we went down to Mumbai Indians who took vengeance for what we did to them in 2010. Despite our loss, Mike Hussey and Dwayne Bravo brought us some happiness by topping the orange cap and purple cap list.

With the fixing scandals surfacing around us, we were dejected. But a loyal fan stands by his team and players through all happy and sad times. We did the same, we had faith in our team and we stood by them. With another three years passing by, we could not afford all our players. While we were really sad to let go of Hussey, Vijay, Badri, Bailey, we were quite happy to welcome our new Super Kings – Dwayne Smith and Brendon Mccullum most importantly. The dividends have paid off as we’ve raced to the play offs yet again.

While there are many memories CSK has given us, here are some of the instances which no Super King can ever forget:

1) Lakshmipathy Balaji and Makhaya Ntini’s hattrick in 2008 against Kings XI and KKR respectively

2) Mike Hussey’s 116 off 54 balls against Kings XI in 2008 and him winning the Orange Cap in 2013

3) Manpreet Gony’s last over heroics against Shoaib Malik, DD in 2008

4) Doug Bollinger’s stunning catch to dismiss Yusuf Pathan, RR in 2010

5) Matthew Hayden’s orange cap in 2009, mongoose assault against DD in 2010, and that match turning catch to dismiss Pollard in an unlikely straight Mid Off position in the 2010 edition finals

6) Subramaniam Badrinath’s heroics as a floater against Kings XI in 2010, against MI in 2012

7) Murali Vijay’s 127 against RR in 2010, 95 vs RCB in 2011 finals, 113 vs DD in 2012 qualifiers

8) Albie Morkel’s 28 run assault on Virat Kohli, RCB in 2012

9) Ravindra Jadeja getting caught at third man off RP Singh, which was a no-ball and gave us victory against RCB in 2013

10) Dwayne Bravo’s last ball six off Rajat Bhatai against KKR in 2012

11) Suresh Raina’s cameo against Kings XI in 2010, fifty against MI in finals in 2010, hundred against Kings XI in 2013

12) Ben Hilfenhaus’ delivery to dismiss Sehwag, DD in 2012

13) Spin twins Ashwin and Murali’s wily off spin resulting in so many 3-fors and 4-fors

14) Faf Du Plessis filling the voids of Mike Hussey and leading from the front in IPL 2012

15) Promising local heroes like Jakati, Tyagi, Mohit, Pandey, Saha

16) Chris Morris’ last over against the KKR in 2013

17) The jaw dropping catches of Hussey, Raina and Faf, and dance moves of Bravo and Vijay

18) MSD’s carnage on Kings XI in 2010 and his ploy to set Hayden at straight-ish mid off to dismiss Pollard in 2010 finals

CSK has built a reputation to take matches till the end. It gives us immense tension, makes us sweat, and heartbeats race, but nine out of ten times we emerge victorious. That sigh of relief cannot be compared to any other feeling. And we the CSKians, have become immune to this situation. We can tackle heart conditions better than anyone else. And despite all the last minute theatrics, CSK also has the reputation of playing fair, having won the Fly Kingfisher Fair Play award 4 out of 6 times.

Our coaching staffs consisting of Stephen Fleming, Steve Rixon, Tommy Simsek, Russel Radhakrishnan and Co. has shown how happy a unit we are. And hence, it comes as no surprise why every player looks very happy in the yellow shades. Every player who is no longer a part of the yellow dressing room has left behind some fond memories here. Our eyes too bleed when we watch some of them play in any color apart from yellow. But even if they do so, we always hope they do well wherever they are, for whichever team they play. A CSKian still cheers for Hussey, Bailey, Vijay, Balaji, and the rest of them.

We are always proud to say that these players once played for us. Under Dhoni; Faf, Bravo and Bailey blossomed as captains for their national sides, and we are very proud of them when we see them sport jerseys for their countries. We have appreciated and always given our opponents their due credits when they beat us. We have come out to cheer in large numbers from across the world for every match.

Six out of eight finals, with two victories and four defeats shows the journey we have had; both happy and sad. But those are just numbers. Supporting a team like CSK and being a part of this journey cannot be described in mere numbers or words. Every moment for us is an emotion, not just a mere feeling. Chennai Super Kings is a family, not just another franchise or a team!