---
title: "Rahul Dravid: The Man Who Deserved A Grand Farewell"
date: 2013-10-13T18:27:27+01:00
description: "The dark knight of Indian cricket and the great wall of India"
---

![alt text](https://i.imgur.com/X5JqOdg.jpg "Rahul Dravid")

Last week, I was in a very big dilemma. It was the finals of Champions League T20 between Rajasthan Royals and Mumbai Indians. There were two legends, both being my favorite, in both the teams: Sachin Tendulkar and Rahul Dravid. While Sachin Tendulkar is about to retire after the West Indies series in November 2013, fans were about to see Rahul Dravid play for one last time. So I was rooting for Rahul Dravid because I felt he is someone who has done so much but not lifted any trophy. Sachin, on the other hand, rightly deserved, had everything in his bag.

Mumbai Indians posted a mammoth total of 202 for the loss of 6 wickets. Many would have lost hope thinking Rajasthan Royals would get crushed. But there came a young hero in the form of Sanju Samson. Eighteen years of age, he took the MI attack to the cleaners. Ajinkya Rahane, complimented Sanju Samson quite well. Rahul Dravid’s face was still watchable for all the fans. But after the dismissal of Sanju Samson, the collapse began. From 117/1, they slumped to 159/6. And then, THE WALL entered for one last time. He just scored a run before getting his stumps disturbed by Coulter-Nile. Sachin was the first man to walk up to him and pat his back and the other MI players followed the same. And in no time, the match got over. The scorecard read, __*“Mumbai Indians beat Rajasthan Royals by 33 runs”*__

![alt text](https://s3.scoopwhoop.com/aka/dravid/4.jpg "Rahul Dravid's final match")

Being a true cricket fan, and especially an Indian, I couldn’t see Dravid’s face. The man who gave everything for his team but the limelight was always hogged by someone else. Though his team lost, I thought he would be carried, or given a guard of honor when he came on to bat. But I was wrong, none happened. It was sad. I seriously felt like crying.

People remember Dravid and Ganguly making their debut at Lord’s in 1996. Ganguly scored a ton and people forgot Dravid’s 95. After India trailed by more than 250 runs in the first innings of the Kolkata Test in 2001, Dravid and Laxman batted the entirety of day four to build up a lead of 384. In the process, Laxman also posted what was, at that time, the highest individual score by an Indian (later to be surpassed by Virender Sehwag’s two triple centuries). It is widely considered to be one of the greatest Test matches in cricket history. Everyone speak about Laxman’s 281, how many speak about Dravid’s 180? Hardly anyone. He scored 145 off 129 balls against Sri Lanka in the same match in which Ganguly scored his highest score of 183 in ODI’s. Again, we all missed him out. He is the only man who has been involved in most number of partnerships than anyone else in the history of Test cricket. He is the only batsman to have faced more than 30,000 deliveries in Test cricket. His 270 against Pakistan, his 233 against the Aussies, his first and last T20 where he hit 3 consecutive sixes off Samit Patel to show why he is sheer class.

Not only a very good player, he was a very good captain as well. It was under him that we won our first test match in South Africa. He was one of the three captains to have won a test series in England. India also won 17 consecutive matches batting second under his belt. He was the vice-captain of the 2003 team which made it to the finals of the World Cup. And in his farewell tournament, he led Rajasthan Royals to the finals and almost clinched the title if not for the blunders made in the last ten overs.

There are many such moments about Dravid which we have forgotten. Moments where we have spoken about others but not Dravid. He kept for us, led the side, stood in the slip cordon for hours, toiled in the hot sun for days, helped us win on many occasions and now, when we look back at what all he did for us and the kind of send off he got, we are all feeling guilty. It is because of people like him cricket is called *The Gentleman’s Game*. He deserved a grand farewell.

Signing off with a famous saying shown below.

![alt text](https://images2.imgbox.com/5f/14/hB6NvH2p_o.jpg "A placard during a match")