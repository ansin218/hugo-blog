---
title: "Tiramisu Recipe"
date: 2018-10-27T17:59:32+02:00
description : "Recipe of my favorite Italian dessert, Tiramisu"
---

![alt text](https://i.imgur.com/2tqIxwL.jpg "Tiramisu")

Tiramisu is my favorite Italian dessert. It is made with spoon biscuits, really soft and has coffee flavored taste to it. I have had it multiple times in Italy from different people. This Tiramisu recipe is from my native Italian friend, Giulia.

## Ingredients:

1. 300g savoiardi cookies (ladyfinger biscuits)
2. 4 fresh eggs
3. 220g mascarpone
4. 500g sugar
5. 100g coffee from the moka già (as much sugar as you want)
6. 300g bitter cocoa powder

## Preparation:

1. Separate the egg white from the egg yolk.
2. Whisk the yolk with half of the sugar until the cream is light and foamy.
3. Wash the whisker and whisk in mascarpone for some time.
4. In another large bowl, beat the egg whites and remaining sugar until stiff and glossy peaks form.
5. Fold the egg whites into mascarpone mixture.
6. Add some milk (just a few drops) to the coffee.
7. Put a layer of a cream in the bottom of an oven pan (30×20 cm), dip savoiardi quickly into the coffee and arrange them in another layer (do not soak the ladyfingers in the coffee for too long or your Tiramisu will turn out soggy).
8. Spread half of the mascarpone mixture on top of the savoiardi.
9. Top with another layer of dipped savoiardi.
10. Spread remaining mascarpone mixture evenly on top of ladyfingers.
11. Level the cream and refrigerate at least 2 hours before serving.
12. Sprinkle with cocoa powder immediately before serving.
