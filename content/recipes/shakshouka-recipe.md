---
title: "Shakshouka Recipe"
date: 2018-12-31T13:53:59+01:00
description : "A breakfast delicacy from Tunisia"
---

![alt text](https://images2.imgbox.com/8c/17/4tPE8MGg_o.jpg "Shakshouka")

I was getting tired of eating things like donuts, krapfens and other sweet things in Europe regularly, and I was looking for breakfast dishes that could be made quickly yet remain at least a bit spicy enough. I stumbled upon a dish called Shakshouka from Tunisia. The variants of Shakshouka are also found in other regions of North Africa and the Middle East. In this blog, I will share this super easy recipe of Shakshouka.

## Ingredients:

1. 2 medium sized potatoes
2. 1 medium sized onions
3. 1 large sized tomato / puree
4. 3 eggs
5. 1 tsp of curmeric powder
6. 1 tsp of coriander powder
7. 1 tsp of cumin powder
8. 1 tsp of chilli powder
9. A pinch of pepper powder
9. 2 green chillies finely chopped or slit
10. Coriander for garnishing

## Preparation:

1. Cut the potatoes into cubes and fry them for 3-5 minutes on medium - high flame.
2. Add the finely chopped onions to this mix along with some salt and a pinch of pepper.
3. Add all the other spices and fry them well for 2-3 minutes.
4. Add the tomato to this, closed the lid of the pan and cook the contents in the pan for a while; perhaps 3-5 minutes.
5. Break and add the eggs directly on top of this mixture and cook until the eggs get cooked according to your liking.
6. Add coriader and the fresh green chillies for garnishing.
7. Enjoy them with bread or just the dish as such.