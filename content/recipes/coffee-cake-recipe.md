---
title: "Coffee Cake Recipe"
date: 2018-11-02T20:21:52+01:00
description: "A simple cake made from coffee"
---

![alt text](https://images2.imgbox.com/3a/69/1xyqTSMM_o.png "Coffee Cake")

Coffee is something which is readily available at supermarkets. It is something that is also common in our kitchen if there is even one person at home drinking coffee. How about we use this to make a coffee cake? This cake is simple and delicious, and has no chocolate or cream. My friends and I usually bake this cake when we crave for cake but do not want to make something fancy.

## Ingredients:

1. 120g flour
2. 3 fresh eggs
3. 60ml sunflower oil
4. 70g baking sugar
5. About 25ml (can be more or less depending on how strong you would like it to be)

## Preparation:

1. Add the sugar and eggs together and form a slightly thick paste with hand mixer.
2. To the paste, add the coffee and continue using the hand mixer for 3-5 minutes.
3. Add the flour to the mixture and mix using a whisker along one direction until no traces of white flour is seen and you feel everything is finely mixed.
4. Put the mixture in the baking tin and bake for around 25-30 minutes in 160 degrees Celsius. 
5. Take it out of the oven when you feel the toothpick has come out clean or bake until the toothpick comes out clean.