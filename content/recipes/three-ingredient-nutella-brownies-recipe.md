---
title: "Three Ingredient Nutella Brownies Recipe"
date: 2018-11-04T11:38:02+01:00
description: "One of the easiest yet tastiest brownies recipe"
---

![alt text](https://images2.imgbox.com/26/5e/tXRuAFtJ_o.jpg "Nutella Brownies")

Feeling lazy to bake but craving for something tasty? The three ingredient brownies recipe made with Nutella comes to rescue. This was surprisingly way too easy to make, took really short time and effort. Here is the recipe below.

## Ingredients:

1. 6 eggs
2. 540g Nutella
3. 180g flour

## Preparation:
1. Use a hand mixer to mix the Nutella and eggs together for about 3-5 minutes.
2. Add the flour to the mixture and fold them using a spatula.
3. Grease your baking tin and pour the mixture into the tin.
4. Bake for around 30 minutes at 170 degrees Celsius. Use a toothpick to check if it comes out clean or bake for a few more minutes accordingly.
