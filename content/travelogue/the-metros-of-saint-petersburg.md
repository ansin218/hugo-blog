---
title: "The Metros Of Saint Petersburg"
date: 2018-05-03T08:27:46+01:00
description: "The stunning amalgam of arts and trains"
---

Sometime in April 2018, I visited Saint Petersburg, Russia. Although there were many things I liked about Saint Petersburg, one that fascinated me a lot was its metro stations. I love trains, be it long distance ones across cities or short distance ones within a city. However, the metros in Saint Petersburg, along with other Russian or Soviet cities perhaps, have arts inculcated in the stations. If sources are to be believed, the ideology behind this was to make sure even the poor could witness some beautiful art; such a lovely thought. I do not have pictures from all the stations but here are some that I would like to share.

![alt text](https://images2.imgbox.com/fb/8d/tQKf9kVX_o.jpg "Some metro station in Saint Petersburg")

![alt text](https://images2.imgbox.com/b0/77/5Puas6nC_o.jpg "Some metro station in Saint Petersburg")

![alt text](https://images2.imgbox.com/d3/79/tS8FmQ50_o.jpg "Some metro station in Saint Petersburg")

![alt text](https://images2.imgbox.com/70/a5/h8a1dg1k_o.jpg "Some metro station in Saint Petersburg")

![alt text](https://images2.imgbox.com/72/f1/WyOpTr9x_o.jpg "Some metro station in Saint Petersburg")

![alt text](https://images2.imgbox.com/c7/b4/IOnwaD8w_o.jpg "Some metro station in Saint Petersburg")

Besides the art, another thing caught my attention: the deep elevators. Some elevators took as long as 1.5-2 minutes one way just go up or down. The deepest one in Admiralteyskaya at 86m took nearly 3 minutes. This is nowhere close to the average time in other cities such as New Delhi, Munich, Paris or Milan. This means, on average, a person in Saint Petersburg spends at least 4 minutes of his/her time in the elevator alone; much more for people who do it more number of times in a day. In addition, the average time and speed between two stops seemed higher than that of Munich and Chennai, the cities where I have lived. However, the trains made a really squeaky annoying sound on arrival. Apart from this sound and the fact the police occassionally checked my bag just because I looked different, I just loved traveling in the metros of Saint Petersburg.

![alt text](https://images2.imgbox.com/3f/5e/B8uz3Y3F_o.jpg "Elevator of some metro station in Saint Petersburg")

A few people from ex-Soviet countries that I know told me this is very normal. For someone like me who has lived and traveled in other parts of the world, this was fascinating. I am looking forward to visit more cities from Russia and ex-Soviet countries.